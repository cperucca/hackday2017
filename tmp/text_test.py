# Import everything needed to edit video clips
from moviepy.editor import *

if __name__ == "__main__":

	filein = './Resources/test_sci.mp4'
	fileout = './Resources/test_out.mp4'
	t1 = 0
	t2 = 20

	# Load myHolidays.mp4 and select the subclip 00:00:50 - 00:00:60
	clip = VideoFileClip(filein).subclip(5,15)

	# Reduce the audio volume (volume x 0.8)
	# clip = clip.volumex(0.8)

	# Generate a text clip. You can customize the font, color, etc.
	txt_clip = TextClip("My Holidays 2013",fontsize=40,color='red')

	# Say that you want it to appear 10s at the center of the screen
	#txt_clip = txt_clip.set_pos('center').set_duration(10)
	# senza duration non funge

	# Overlay the text clip on the first video clip
	video = CompositeVideoClip([clip, txt_clip])

	# Write the result to a file
	video.write_videofile("myHolidays_edited.mp4",fps=24, codec='mpeg4')
