import codecs
import datetime

TEMPLATE_DIR = '/home/perucccl/HackDay2017/Templates'

def SecToTC( tc_in_sec ):


    millis = ''
    print ( type(tc_in_sec))
    if 'str' in str(type(tc_in_sec)):
        tc_in_sec = int(tc_in_sec)
    else:
        if 'float' in str(type(tc_in_sec)):
            # per prendere anche i millisecondi
            # dovrei splittare per . e vedere se esiste parte 
            # dopo virgola la riattacco alla fine della stringa 
            # di timecode
            millis = '.' + str(tc_in_sec).split('.')[-1]
            tc_in_sec = int(tc_in_sec)
	


    mins, secs = divmod(tc_in_sec, 60)
    hours, mins = divmod(mins, 60)
    return '%02d:%02d:%02d%s' % (hours, mins, secs, millis)

'''
if __name__ == "__main__":
	print ( SecToTC( 20.10 ) )
	print ( SecToTC( 25 ) )
	print ( SecToTC( 65.30 ) )
	print ( SecToTC( 265 ) )
	exit(0)
'''
def Second_Conversion(x): #Takes a string such as "2:22:04.996304"

    HMS = x.split(':')
    SF = x.split('.')
    Full_HMS = x[0:7]
    Split_HMS = Full_HMS.split(':')

    Full_Units = int(Split_HMS[0]) * 3600 + int(Split_HMS[1]) * 60 + int(Split_HMS[2])


    Second_Fractions = int(SF[1])
    Converted_Seconds_Fractions = Second_Fractions /1000000
    print(Converted_Seconds_Fractions)

    Final_Time = Full_Units + Converted_Seconds_Fractions
    print(Final_Time)

def TcToSec_Ms( tc_in_tc ):

	time_string = "17:48:12.98"
	t = datetime.datetime.strptime(tc_in_tc, "%H:%M:%S.%f")
	print ( t )
	seconds = 60 * t.minute * t.hour

	print (seconds, t.microsecond)

	return str(seconds) + '.' + str(t.microsecond)

if __name__ == "__main__":
	print ( TcToSec_Ms("00:01:12.98"))
	print ( Second_Conversion("00:01:12.98"))
	exit(0)



def Durata( tc_in_sec, tc_out_sec ):

	dur = int(tc_out_sec) - int(tc_in_sec)
	# deve ritornare la durata del pezzo in millisecondi
	return dur


def Durata_in_ms( tc_in_sec, tc_out_sec ):

	dur = float(tc_out_sec) - float(tc_in_sec)
	# deve ritornare la durata del pezzo in millisecondi
	dur =  dur * 1000.0
	return str(dur)

def CreaData():
	# devo creare la data per il nome 
	# 20170912 -	
	now = datetime.datetime.now()
	return now.strftime('%Y%m%d')

def Crea_SubTitles(lista, ece_id):

	nome_file_out = '/mnt/subtitles/2017/ts_DATA_i_ECEID.xml'
	nome_file_out = nome_file_out.replace('DATA', CreaData()).replace('ECEID', ece_id)

	# mi arriva una lista tipo 
	#lista = [['ID_DEL_JOB_PER_GIULIANO'], [['8632555', '10', '15', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289986.mp4', 'Telegiornale', './Chops/5h9x2xc5.mp4'], ['8631973', '20', '25', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289918.mp4', 'Telegiornale', './Chops/msdbmja8.mp4'], ['8632555', '15', '25', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289986.mp4', 'Telegiornale', './Chops/zyjipd3v.mp4'], ['8631973', '00', '10', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289918.mp4', 'Telegiornale', './Chops/a942a26u.mp4']]]
	print ( ' in crea Subs')
	lista = lista['video']

	fout = codecs.open(nome_file_out, 'w', 'utf-8')
	print ( 'Creo il file di subtitles : ' + nome_file_out )

	fin = codecs.open( TEMPLATE_DIR + '/TEMPLATE_INIZIO.xml', 'r', 'utf-8')
	fout.write(fin.read())
	fin.close()


	fin = codecs.open( TEMPLATE_DIR + '/TEMPLATE_BLOCK.xml', 'r', 'utf-8')
	testo_template = fin.read()
	count = 0
	tmp_tc_in = 0
	tmp_tc_out = 0
	durata = 0
	for lis in lista:
		#print ( str(count).zfill(4))
		print (lis[1],lis[2], lis[4] )
		count += 1
		durata = Durata(lis[1],lis[2])
		tmp_tc_out = tmp_tc_in + durata
		testo_da_scrivere = testo_template.replace('__REPLACE_NUM_PROGRESSIVO__', str(count).zfill(4)).replace('__REPLACE_TITOLO__', lis[4]).replace('__REPLACE_TC_IN__', SecToTC(tmp_tc_in)).replace('__REPLACE_TC_END__', SecToTC(tmp_tc_out).replace('__REPLACE_DURATA__', Durata_in_ms(lis[1],lis[2])))
		fout.write( testo_da_scrivere.replace('__REPLACE_DURATA__', Durata_in_ms(lis[1],lis[2])))
		tmp_tc_in = tmp_tc_out

	fin = codecs.open( TEMPLATE_DIR + '/TEMPLATE_FINE.xml', 'r', 'utf-8')
	fout.write(fin.read())
	fin.close()
	fout.close()

if __name__ == "__main__":

	lista = [['ID_DEL_JOB_PER_GIULIANO'], [['8632555', '10', '15', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289986.mp4', 'Telegiornale', './Chops/5h9x2xc5.mp4'], ['8631973', '20', '25', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289918.mp4', 'Telegiornale', './Chops/msdbmja8.mp4'], ['8632555', '15', '25', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289986.mp4', 'Telegiornale', './Chops/zyjipd3v.mp4'], ['8631973', '00', '10', '/mnt/rsi_transcoded/vmeo/httpd/html//rsi/unrestricted/2017/01/26/2289918.mp4', 'Telegiornale', './Chops/a942a26u.mp4']]]
	Crea_SubTitles(lista, 'dummy_ece_id')
