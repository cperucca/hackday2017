import urllib, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import shutil
import glob
import json

from urllib.parse import urlencode
from urllib.request import Request, urlopen

def Prendi_Hbbtv_Json( ece_id ):
	
	# devo prendere il json dalla url http://www.rsi.ch/rsi-api/intlay/hbbtv/play/

        base64string = base64.encodestring(('%s:%s' % ('perucccl', 'perucccl')).encode()).decode().replace('\n', '')
        url = "http://www.rsi.ch/rsi-api/intlay/hbbtv/play/" + str(ece_id)

        request = Request(url)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'GET'
        result = urlopen(request)

        #print (result.read())
        return result


def Prendi_File_Path( ece_id ):
	
	# devo prendere il json dalla url http://www.rsi.ch/rsi-api/intlay/hbbtv/play/

        base64string = base64.encodestring(('%s:%s' % ('perucccl', 'perucccl')).encode()).decode().replace('\n', '')
        url = "http://www.rsi.ch/rsi-api/intlay/hbbtv/play/" + str(ece_id)

        request = Request(url)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'GET'
        result = urlopen(request)

        #print (result.read())
        return result



def Create_Bin_Content( filename ):

        base64string = base64.encodestring(('%s:%s' % ('perucccl', 'perucccl')).encode()).decode().replace('\n', '')
        file = open(filename, 'rb')
        data = file.read()
        url = "http://internal.publishing.production.rsi.ch/webservice/escenic/binary/"

        request = urllib.Request(url, data=data)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'POST'
        result = urllib2.urlopen(request)

        #print (result.read())
        return result

def Scegli_Versione( lista_pod ):

	for pod in lista_pod:
		#print ( pod['width'] ) 
		#print ( '/' + pod['uri'].split('rsi.ch/')[-1] ) 
		if 1280 == pod['width']:
			return '/' + pod['uri'].split('rsi.ch/')[-1] 

	return 'ERROR'

def Prendi_File_Path( ece_id):
	
	_json_del_video = Prendi_Hbbtv_Json( ece_id )
	_json_del_video = _json_del_video.read().decode('UTF-8')
	_json_del_video = _json_del_video.replace('\n','')
	#print (_json_del_video)
	data = json.loads( _json_del_video )
	#print (data['Videos'])
	#print (data['Videos']['Video'][0]['AssetSet']['location']['podcast'])
	uri = Scegli_Versione( data['Videos']['Video'][0]['AssetSet']['location']['podcast'] )
	#print (Scegli_Versione( data['Videos']['Video'][0]['AssetSet']['location']['podcast'] ))
	titolo = data['Videos']['Video'][0]['Show']['title']
	#print (data['Videos']['Video'][0]['Show']['title'])
	return [ uri, titolo ]
	


if __name__ == "__main__":

	

	print (Prendi_File_Path( 8632555 ))
	
	# Create_Bin_Content( '../Resources/test_sci.mp4')
