
per prendere le info del video usando l'ID 
http://www.rsi.ch/rsi-api/intlay/hbbtv/play/7185794

con dentro 

"Video": [
	{
	"AssetSet": {
		"expireDate": "2199-01-01 13:00:00 +0100",
		"homeSection": "Telegiornale",
		"location": {
			"podcast": [
				{
					"height": 272,
					"mimeType": "video/mp4",
					"uri": "http://mediaww.rsi.ch/rsi/unrestricted/2016/04/13/2096260.mp4",
					"width": 480
				},
				{
					"height": 360,
					"mimeType": "video/mp4",
					"uri": "http://mediaww.rsi.ch/rsi/unrestricted/2016/04/13/2096261.mp4",
					"width": 640
				}

da cui devo prendere quello con la width maggiore

in Service/pyGet_Video_Info.py ho a disposizione la Prendi_File_Path che ritorna
file path e titolo

il path per prendere i files
/mnt/rsi_transcoded/vmeo/httpd/html/rsi/unrestricted/2016/04/13/2096260.mp4

/* TAGLIO e incollaggio */
usando la libreria pyMachete.py ho a disposizione taglia e incolla
da sistemare i valori di ritorno.


/* inserimento */
prendo il file generato e lo uploado con 
curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml" --upload-file NOME_FILE  http://internal.publishing.production.rsi.ch/webservice/escenic/binary

che mi restituisce qualcosa tipo:
http://internal.publishing.production.rsi.ch/webservice/escenic/binary/-1/2017/1/24/16/76c1494b-b2ca-45d2-8e48-3df062550f0d.bin

che e' quello che devo mettere editando il TEMPLATE_TRANSCODABLE.xml 
e mettendolo nel field binary assieme a titolo e altre amenita'
incluso il nome del file di sottotitoli nel campo GoogleVideoDescription

che poi uploado sulla url
curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml" 'http://internal.publishing2.production.rsi.ch/webservice/escenic/section/5909/content-items' --upload-file TEMPLATE_TRANSCODABLE.xml

Ocio alla sezione che sia quella giusta.


mentre i sottotitoli sono 
/mnt/subtitles/2017/

con nome ts_DATA_i_ECEID.xml

e con  formato

<tt xmlns="http://www.w3.org/ns/ttml" xml:lang="en" xsi:schemaLocation="http://www.w3.org/ns/ttml ttaf1-dfxp.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tts="http://www.w3.org/ns/ttml#styling">
  <body>
    <div>

      <p xml:id="ID_0000" begin="00:00:00.00" end="00:00:20.10" dur="2000ms">
        <span tts:color="blue" tts:backgroundColor="yellow" tts:fontSize="1">      Test Subtitles Blue   </span>
      </p>

      <p xml:id="ID_0001" begin="00:00:24.10" end="00:00:44.10" dur="2000ms">
        <span tts:color="white" tts:backgroundColor="black" tts:fontSize="1">     Test Subtitles White.  </span>
      </p>

    </div>
  </body>
</tt>

con quindi id - begin - end - dur - e testo da templetizzare
e creare accordingly

quindi con i 4 zeri di "ID_0000" da cambiare con un numerale progressivo

 <p xml:id="ID___REPLACE_NUM_PROGRESSIVO__" begin="__REPLACE_TC_IN__" end="__REPLACE_TC_END__" dur="__REPLACE_DURATA__ms">

ocio alla durate in milliseconds

        <span tts:color="blue" tts:backgroundColor="yellow" tts:fontSize="1">    __REPLACE_TITOLO__   </span>
 dove va il titolo dello spezzone

TEMPLATE_INIZIO.xml
TEMPLATE_FINE.xml
TEMPLATE_BLOCK.xml



